#PI CIRCULO

import numpy as np
import matplotlib.pyplot as plt

L=lambda x,y,x0,y0:np.sqrt(((x-x0)**2)+((y-y0)**2))

r=float(input("Ingrese el radio: "))
f=(np.pi)/180
Xc=[]
Yc=[]
for k in range(0,360,1):
	Xc.append(r*np.cos(f*k))
	Yc.append(r*np.sin(f*k))
print("")
print("Datos de la circunferencia llenos")


N=input("Ingrese los lados del poligono: ")
d=(2*np.pi)/(float(N))
Xp=[]
Yp=[]
for k in range(N+1):
	Xp.append(r*np.cos(d*k))
	Yp.append(r*np.sin(d*k))
print("Datos llenos del poligono llenos")
print("")
t=0
B=0
for h in range(1,N,1):
	t=L(Xp[h],Yp[h],Xp[h-1],Yp[h-1])+t
print("La longitud es: ")
print(t)

if r==0.5: 
	print(t)
	print("La diferencia es: ")
	print(np.pi-t)
else:
	B=t/(2*r)
	print(B)
	print("La diferencia es: ")
	print(np.pi-B)


plt.plot(Xc,Yc)
plt.plot(Xp,Yp)
plt.show()